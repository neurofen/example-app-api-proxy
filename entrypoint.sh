#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

## docker recommends running the primary application in the foreground.
nginx -g 'daemon off;'